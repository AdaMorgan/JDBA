package core.connect;

import core.SQLBuilder;
import core.log.Logger;
import org.jetbrains.annotations.NotNull;

import java.sql.Connection;
import java.sql.SQLException;

public class Connect {
    private final SQLBuilder builder;
    private final int timeout;
    private final Logger logger;
    private final Connection connect;

    public Connect(@NotNull SQLBuilder builder) throws SQLException {
        this.builder = builder;
        this.connect = builder.build();
        this.timeout = builder.timeout;
        this.logger = builder.logger;
    }

    public boolean isValid() throws SQLException {
        return this.connect.isValid(this.timeout);
    }
}
