package core;

import core.log.Logger;
import org.jetbrains.annotations.NotNull;

import java.sql.Connection;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

public class SQLBuilder {
    protected final List<Object> listeners = new LinkedList<>();
    private boolean loggerState = true;
    private boolean pool = false;

    public int timeout = 4000;
    public Logger logger;
    private String user, password, server, database;
    private int port;

    private SQLBuilder() {
        this.logger = new Logger(this, loggerState);
    }

    @NotNull
    public static SQLBuilder create() {
        return new SQLBuilder();
    }

    @NotNull
    public SQLBuilder setUser(String user) {
        this.user = user;
        return this;
    }

    @NotNull
    public SQLBuilder setPassword(String password) {
        this.password = password;
        return this;
    }

    @NotNull
    public SQLBuilder setDatabase(String database) {
        this.database = database;
        return this;
    }

    @NotNull
    public SQLBuilder setServer(String server) {
        this.server = server;
        return this;
    }

    @NotNull
    public SQLBuilder setPort(int port) {
        this.port = port;
        return this;
    }

    @NotNull
    public SQLBuilder setLogger(boolean state) {
        this.loggerState = state;
        return this;
    }

    @NotNull
    public SQLBuilder setValid(int timeout) {
        this.timeout = timeout;
        return this;
    }

    @NotNull
    public SQLBuilder setPoolConnected(boolean state) {
        this.pool = state;
        return this;
    }

    @NotNull
    public SQLBuilder addEventListener(@NotNull Object... listeners) {
        Collections.addAll(this.listeners, listeners);
        return this;
    }

    @NotNull
    public SQLBuilder removeEventListener(@NotNull Object... listeners) {
        this.listeners.removeAll(List.of(listeners));
        return this;
    }

    public Connection build() {
        return create().build();
    }
}
